<?php

namespace App\Form;

use App\Entity\Caisse;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CaisseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('caisseNumOperation', TextType::class,array(
                'label' => 'N° Opération',
                'attr' => array('class'=>'form-control input--style-4', 'style'=>'background-color:lightgrey;', 'readonly'=>'readonly')

            ))
            ->add('caisseTypeOperation', ChoiceType::class, array(
                'label' => 'Type D opération',
                'attr' => array('class'=>'form-control input--style-4'),
                'choices' =>[
                    'Entrée' => 'entrée',
                    'Sortie' => 'sortie'
                ]
            ))
            ->add('caisseDate', TextType::class, array(
                'label' => 'Date',
                'attr' => array('class'=>'form-control input--style-4 js-datepicker')
            ))
            ->add('caisseNumFacture', TextType::class, array(
                'label' => 'N° Facture',
                'attr' => array('class'=>'form-control input--style-4')
            ))
            ->add('caisseLibelle', TextType::class, array(
                'label' => 'Libellé',
                'attr' => array('class'=>'form-control input--style-4')
            ))
            ->add('caisseMontant', TextType::class, array(
                'label' => 'Montant',
                'attr' => array('class'=>'form-control input--style-4')
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Caisse::class,
        ]);
    }
}
