<?php

namespace App\Form;

use App\Entity\Charge;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Charge1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('chargeNumOperation', TextType::class, array(
                'label' => 'N° Operation',
                'attr' => array('class'=>'form-control input--style-4', 'style'=>'background-color:lightgrey;', 'readonly'=>'readonly')
            ))
            ->add('chargeNature', ChoiceType::class, array(
                'label' => 'Nature',
                'attr' => array('class'=>'form-control input--style-4'),
                'choices' => array(
                    'Fixe' => 'fixe',
                    'Variable' => 'variable',
                )
            ))
            ->add('cahrgeDate', TextType::class, array(
                'label' => 'Date',
                'attr' => array('class'=>'form-control input--style-4 js-datepicker')
            ))
            ->add('chargeEcheance', TextType::class, array(
                'label' => 'Echéance',
                'attr' => array('class'=>'form-control input--style-4')
            ))
            ->add('chargeLibelle', TextType::class, array(
                'label' => 'Libellé',
                'attr' => array('class'=>'form-control input--style-4')
            ))
            ->add('chargeMontant', TextType::class, [
                'label' => 'Montant',
                'attr' => array('class'=>'form-control input--style-4')
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Charge::class,
        ]);
    }
}
