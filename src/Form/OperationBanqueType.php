<?php

namespace App\Form;

use App\Entity\OperationBanque;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OperationBanqueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('operationBanqueNum', TextType::class, [
                'label' => 'N° Opération',
                'attr' => array('class'=>'form-control input--style-4', 'style'=>'background-color:lightgrey;', 'readonly'=>'readonly')
            ])
            ->add('operationBanqueType', ChoiceType::class, [
                'label' => 'Type D opération',
                'attr' => array('class'=>'form-control input--style-4'),
                'choices' => [
                    'Entrée' => 'entrée',
                    'Sortie' => 'sortie'
                ]
            ])
            ->add('operationBanqueDate', TextType::class, [
                'label' => 'Date',
                'attr' => array('class'=>'form-control input--style-4 js-datepicker')
            ])
            ->add('operationBanqueNumFacture', TextType::class, [
                'label' => 'N° Facture',
                'attr' => array('class'=>'form-control input--style-4')
            ])
            ->add('operationBanqueLibelle', TextType::class, [
                'label' => 'Libelllé',
                'attr' => array('class'=>'form-control input--style-4')
            ])
            ->add('operationBanqueMontant', TextType::class, [
                'label' => 'Montant',
                'attr' => array('class'=>'form-control input--style-4')
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OperationBanque::class,
        ]);
    }
}
