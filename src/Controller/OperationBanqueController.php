<?php

namespace App\Controller;

use App\Entity\OperationBanque;
use App\Form\OperationBanqueType;
use App\Repository\OperationBanqueRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/operation/banque")
 */
class OperationBanqueController extends AbstractController
{
    /**
     * @Route("/", name="operation_banque_index", methods={"GET"})
     */
    public function index(OperationBanqueRepository $operationBanqueRepository): Response
    {
        $totaleOperationBanqueEntree = $operationBanqueRepository->findAllEntree();
        $totaleOperationBanqueSortie = $operationBanqueRepository->findAllSortie();
        return $this->render('operation_banque/index.html.twig', [
            'operation_banques' => $operationBanqueRepository->findAll(),
            'totalEntree' => $totaleOperationBanqueEntree,
            'totalSortie' => $totaleOperationBanqueSortie
        ]);
    }

    /**
     * @Route("/new", name="operation_banque_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $operationBanque = new OperationBanque();
        $form = $this->createForm(OperationBanqueType::class, $operationBanque);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($operationBanque);
            $entityManager->flush();

            return $this->redirectToRoute('operation_banque_index');
        }

        return $this->render('operation_banque/new.html.twig', [
            'operation_banque' => $operationBanque,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="operation_banque_show", methods={"GET"})
     */
    public function show(OperationBanque $operationBanque): Response
    {
        return $this->render('operation_banque/show.html.twig', [
            'operation_banque' => $operationBanque,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="operation_banque_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, OperationBanque $operationBanque): Response
    {
        $form = $this->createForm(OperationBanqueType::class, $operationBanque);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('operation_banque_index');
        }

        return $this->render('operation_banque/edit.html.twig', [
            'operation_banque' => $operationBanque,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="operation_banque_delete", methods={"DELETE"})
     */
    public function delete(Request $request, OperationBanque $operationBanque): Response
    {
        if ($this->isCsrfTokenValid('delete'.$operationBanque->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($operationBanque);
            $entityManager->flush();
        }

        return $this->redirectToRoute('operation_banque_index');
    }
}
