<?php

namespace App\Controller;

use App\Entity\Caisse;
use App\Form\CaisseType;
use App\Repository\CaisseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/caisse")
 */
class CaisseController extends AbstractController
{
    /**
     * @Route("/", name="caisse_index", methods={"GET"})
     */
    public function index(CaisseRepository $caisseRepository): Response
    {
        $totaleCaisseEntree = $caisseRepository->findAllEntree();
        $totaleCaisseSortie = $caisseRepository->findAllSortie();
        return $this->render('caisse/liste.html.twig', [
            'caisses' => $caisseRepository->findAll(),
            'totalEntree' => $totaleCaisseEntree,
            'totalSortie' => $totaleCaisseSortie
        ]);
    }

    /**
     * @Route("/new", name="caisse_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $caisse = new Caisse();
        $form = $this->createForm(CaisseType::class, $caisse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($caisse);
            $entityManager->flush();

            return $this->redirectToRoute('caisse_index');
        }

        return $this->render('caisse/new.html.twig', [
            'caisse' => $caisse,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="caisse_show", methods={"GET"})
     */
    public function show(Caisse $caisse): Response
    {
        return $this->render('caisse/show.html.twig', [
            'caisse' => $caisse,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="caisse_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Caisse $caisse): Response
    {
        $form = $this->createForm(CaisseType::class, $caisse);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('caisse_index');
        }

        return $this->render('caisse/edit.html.twig', [
            'caisse' => $caisse,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="caisse_delete", methods={"DELETE"})
     */
    public function delete(Caisse $caisse): Response
    {
        if ($caisse) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($caisse);
            $em->flush();
            return $this->redirectToRoute('caisse_index');
        }

        throw new NotFoundHttpException('Article introuvable');
    }


}
