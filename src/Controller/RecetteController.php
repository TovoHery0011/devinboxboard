<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class RecetteController extends AbstractController
{
    /**
     * @Route("/recette", name="recette")
     */
    public function index()
    {
        return $this->render('recette/index.html.twig', [
            'controller_name' => 'RecetteController',
        ]);
    }

    /**
     * @Route("/factureation", name="facturation")
     */
    public function facturation()
    {
        return $this->render('recette/facturation.html.twig', [
            'controller_name' => 'RecetteController',
        ]);
    }
}
