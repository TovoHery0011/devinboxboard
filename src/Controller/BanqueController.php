<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class BanqueController extends AbstractController
{
    /**
     * @Route("/mouvement_Banque", name="mouvement_Banque")
     */
    public function index()
    {
        return $this->render('banque/mouvement_Banque.html.twig', [
            'controller_name' => 'BanqueController',
        ]);
    }
}
