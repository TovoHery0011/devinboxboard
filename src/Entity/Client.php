<?php

namespace App\Entity;

use App\Repository\ClientRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ClientRepository::class)
 */
class Client
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $client_nom;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $client_contact;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $client_adresse;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getClientNom(): ?string
    {
        return $this->client_nom;
    }

    public function setClientNom(?string $client_nom): self
    {
        $this->client_nom = $client_nom;

        return $this;
    }

    public function getClientContact(): ?string
    {
        return $this->client_contact;
    }

    public function setClientContact(?string $client_contact): self
    {
        $this->client_contact = $client_contact;

        return $this;
    }

    public function getClientAdresse(): ?string
    {
        return $this->client_adresse;
    }

    public function setClientAdresse(string $client_adresse): self
    {
        $this->client_adresse = $client_adresse;

        return $this;
    }
}
