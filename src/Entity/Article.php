<?php

namespace App\Entity;

use App\Repository\ArticleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ArticleRepository::class)
 */
class Article
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $article_libelle;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $article_code;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $article_qte;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $article_unite;

    /**
     * @ORM\Column(type="float")
     */
    private $article_pu;

    /**
     * @ORM\Column(type="float")
     */
    private $article_total;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $article_num_operation;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $article_nom;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $article_fournisseur;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $article_date;

    /**
     * @ORM\Column(type="float")
     */
    private $article_pu_achat;

    /**
     * @ORM\Column(type="float")
     */
    private $article_pu_vente;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArticleLibelle(): ?string
    {
        return $this->article_libelle;
    }

    public function setArticleLibelle(string $article_libelle): self
    {
        $this->article_libelle = $article_libelle;

        return $this;
    }

    public function getArticleCode(): ?string
    {
        return $this->article_code;
    }

    public function setArticleCode(string $article_code): self
    {
        $this->article_code = $article_code;

        return $this;
    }

    public function getArticleQte(): ?string
    {
        return $this->article_qte;
    }

    public function setArticleQte(string $article_qte): self
    {
        $this->article_qte = $article_qte;

        return $this;
    }

    public function getArticleUnite(): ?string
    {
        return $this->article_unite;
    }

    public function setArticleUnite(string $article_unite): self
    {
        $this->article_unite = $article_unite;

        return $this;
    }

    public function getArticlePu(): ?float
    {
        return $this->article_pu;
    }

    public function setArticlePu(float $article_pu): self
    {
        $this->article_pu = $article_pu;

        return $this;
    }

    public function getArticleTotal(): ?float
    {
        return $this->article_total;
    }

    public function setArticleTotal(float $article_total): self
    {
        $this->article_total = $article_total;

        return $this;
    }

    public function getArticleNumOperation(): ?string
    {
        return $this->article_num_operation;
    }

    public function setArticleNumOperation(string $article_num_operation): self
    {
        $this->article_num_operation = $article_num_operation;

        return $this;
    }

    public function getArticleNom(): ?string
    {
        return $this->article_nom;
    }

    public function setArticleNom(string $article_nom): self
    {
        $this->article_nom = $article_nom;

        return $this;
    }

    public function getArticleFournisseur(): ?string
    {
        return $this->article_fournisseur;
    }

    public function setArticleFournisseur(string $article_fournisseur): self
    {
        $this->article_fournisseur = $article_fournisseur;

        return $this;
    }

    public function getArticleDate(): ?string
    {
        return $this->article_date;
    }

    public function setArticleDate(string $article_date): self
    {
        $this->article_date = $article_date;

        return $this;
    }

    public function getArticlePuAchat(): ?float
    {
        return $this->article_pu_achat;
    }

    public function setArticlePuAchat(float $article_pu_achat): self
    {
        $this->article_pu_achat = $article_pu_achat;

        return $this;
    }

    public function getArticlePuVente(): ?float
    {
        return $this->article_pu_vente;
    }

    public function setArticlePuVente(float $article_pu_vente): self
    {
        $this->article_pu_vente = $article_pu_vente;

        return $this;
    }
}
