<?php

namespace App\Entity;

use App\Repository\PayementRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PayementRepository::class)
 */
class Payement
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $payement_libelle;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPayementLibelle(): ?string
    {
        return $this->payement_libelle;
    }

    public function setPayementLibelle(string $payement_libelle): self
    {
        $this->payement_libelle = $payement_libelle;

        return $this;
    }
}
