<?php

namespace App\Entity;

use App\Repository\FactureHasArticleRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FactureHasArticleRepository::class)
 */
class FactureHasArticle
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $facture_facture_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $facture_payement_payement_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $article_article_id;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFactureFactureId(): ?int
    {
        return $this->facture_facture_id;
    }

    public function setFactureFactureId(int $facture_facture_id): self
    {
        $this->facture_facture_id = $facture_facture_id;

        return $this;
    }

    public function getFacturePayementPayementId(): ?int
    {
        return $this->facture_payement_payement_id;
    }

    public function setFacturePayementPayementId(int $facture_payement_payement_id): self
    {
        $this->facture_payement_payement_id = $facture_payement_payement_id;

        return $this;
    }

    public function getArticleArticleId(): ?int
    {
        return $this->article_article_id;
    }

    public function setArticleArticleId(int $article_article_id): self
    {
        $this->article_article_id = $article_article_id;

        return $this;
    }
}
