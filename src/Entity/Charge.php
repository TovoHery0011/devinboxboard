<?php

namespace App\Entity;

use App\Repository\ChargeRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ChargeRepository::class)
 */
class Charge
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45 , nullable=true)
     */
    private $chargeNumOperation;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $chargeNature;

    /**
     * @ORM\Column(type="string")
     */
    private $cahrgeDate;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $chargeEcheance;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $chargeLibelle;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $chargeMontant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getChargeNumOperation(): ?string
    {
        return $this->chargeNumOperation;
    }

    public function setChargeNumOperation(string $chargeNumOperation): self
    {
        $this->chargeNumOperation = $chargeNumOperation;

        return $this;
    }

    public function getChargeNature(): ?string
    {
        return $this->chargeNature;
    }

    public function setChargeNature(string $chargeNature): self
    {
        $this->chargeNature = $chargeNature;

        return $this;
    }

    public function getCahrgeDate(): ?string
    {
        return $this->cahrgeDate;
    }

    public function setCahrgeDate(string $cahrgeDate): self
    {
        $this->cahrgeDate = $cahrgeDate;

        return $this;
    }

    public function getChargeEcheance(): ?string
    {
        return $this->chargeEcheance;
    }

    public function setChargeEcheance(string $chargeEcheance): self
    {
        $this->chargeEcheance = $chargeEcheance;

        return $this;
    }

    public function getChargeLibelle(): ?string
    {
        return $this->chargeLibelle;
    }

    public function setChargeLibelle(string $chargeLibelle): self
    {
        $this->chargeLibelle = $chargeLibelle;

        return $this;
    }

    public function getChargeMontant(): ?string
    {
        return $this->chargeMontant;
    }

    public function setChargeMontant(string $chargeMontant): self
    {
        $this->chargeMontant = $chargeMontant;

        return $this;
    }
}
