<?php

namespace App\Entity;

use App\Repository\CaisseRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CaisseRepository::class)
 */
class Caisse
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $caisseNumOperation;

    /**
     * @ORM\Column(type="string")
     */
    private $caisseTypeOperation;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $caisseDate;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $caisseNumFacture;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $caisseLibelle;

    /**
     * @ORM\Column(type="float")
     */
    private $caisseMontant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCaisseNumOperation(): ?string
    {
        return $this->caisseNumOperation;
    }

    public function setCaisseNumOperation(string $caisseNumOperation): self
    {
        $this->caisseNumOperation = $caisseNumOperation;

        return $this;
    }

    public function getCaisseTypeOperation(): ?string
    {
        return $this->caisseTypeOperation;
    }

    public function setCaisseTypeOperation(string $caisseTypeOperation): self
    {
        $this->caisseTypeOperation = $caisseTypeOperation;

        return $this;
    }

    public function getCaisseDate(): ?string
    {
        return $this->caisseDate;
    }

    public function setCaisseDate(string $caisseDate): self
    {
        $this->caisseDate = $caisseDate;

        return $this;
    }

    public function getCaisseNumFacture(): ?string
    {
        return $this->caisseNumFacture;
    }

    public function setCaisseNumFacture(string $caisseNumFacture): self
    {
        $this->caisseNumFacture = $caisseNumFacture;

        return $this;
    }

    public function getCaisseLibelle(): ?string
    {
        return $this->caisseLibelle;
    }

    public function setCaisseLibelle(string $caisseLibelle): self
    {
        $this->caisseLibelle = $caisseLibelle;

        return $this;
    }

    public function getCaisseMontant(): ?float
    {
        return $this->caisseMontant;
    }

    public function setCaisseMontant(float $caisseMontant): self
    {
        $this->caisseMontant = $caisseMontant;

        return $this;
    }
}
