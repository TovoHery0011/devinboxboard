<?php

namespace App\Entity;

use App\Repository\OperationBanqueRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OperationBanqueRepository::class)
 */
class OperationBanque
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     */
    private $operationBanqueNum;

    /**
     * @ORM\Column(type="string")
     */
    private $operationBanqueType;

    /**
     * @ORM\Column(type="string")
     */
    private $operationBanqueDate;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $operationBanqueNumFacture;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $operationBanqueLibelle;

    /**
     * @ORM\Column(type="float")
     */
    private $operationBanqueMontant;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOperationBanqueNum(): ?string
    {
        return $this->operationBanqueNum;
    }

    public function setOperationBanqueNum(string $operationBanqueNum): self
    {
        $this->operationBanqueNum = $operationBanqueNum;

        return $this;
    }

    public function getOperationBanqueType(): ?string
    {
        return $this->operationBanqueType;
    }

    public function setOperationBanqueType(string $operationBanqueType): self
    {
        $this->operationBanqueType = $operationBanqueType;

        return $this;
    }

    public function getOperationBanqueDate(): ?string
    {
        return $this->operationBanqueDate;
    }

    public function setOperationBanqueDate(string $operationBanqueDate): self
    {
        $this->operationBanqueDate = $operationBanqueDate;

        return $this;
    }

    public function getOperationBanqueNumFacture(): ?string
    {
        return $this->operationBanqueNumFacture;
    }

    public function setOperationBanqueNumFacture(string $operationBanqueNumFacture): self
    {
        $this->operationBanqueNumFacture = $operationBanqueNumFacture;

        return $this;
    }

    public function getOperationBanqueLibelle(): ?string
    {
        return $this->operationBanqueLibelle;
    }

    public function setOperationBanqueLibelle(string $operationBanqueLibelle): self
    {
        $this->operationBanqueLibelle = $operationBanqueLibelle;

        return $this;
    }

    public function getOperationBanqueMontant(): ?float
    {
        return $this->operationBanqueMontant;
    }

    public function setOperationBanqueMontant(float $operationBanqueMontant): self
    {
        $this->operationBanqueMontant = $operationBanqueMontant;

        return $this;
    }
}
