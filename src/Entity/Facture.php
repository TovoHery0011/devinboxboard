<?php

namespace App\Entity;

use App\Repository\FactureRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FactureRepository::class)
 */
class Facture
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $facture_date;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $facture_sous_total;

    /**
     * @ORM\Column(type="float")
     */
    private $facture_total;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFactureDate(): ?string
    {
        return $this->facture_date;
    }

    public function setFactureDate(string $facture_date): self
    {
        $this->facture_date = $facture_date;

        return $this;
    }

    public function getFactureSousTotal(): ?string
    {
        return $this->facture_sous_total;
    }

    public function setFactureSousTotal(string $facture_sous_total): self
    {
        $this->facture_sous_total = $facture_sous_total;

        return $this;
    }

    public function getFactureTotal(): ?float
    {
        return $this->facture_total;
    }

    public function setFactureTotal(float $facture_total): self
    {
        $this->facture_total = $facture_total;

        return $this;
    }
}
