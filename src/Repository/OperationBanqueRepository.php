<?php

namespace App\Repository;

use App\Entity\OperationBanque;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OperationBanque|null find($id, $lockMode = null, $lockVersion = null)
 * @method OperationBanque|null findOneBy(array $criteria, array $orderBy = null)
 * @method OperationBanque[]    findAll()
 * @method OperationBanque[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OperationBanqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OperationBanque::class);
    }

    // /**
    //  * @return OperationBanque[] Returns an array of OperationBanque objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OperationBanque
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAllEntree()
    {
        $query = $this->createQueryBuilder('o');
        $query
            ->select('SUM(o.operationBanqueMontant) as totalEntree')
            ->where('o.operationBanqueType =:type')
            ->setParameter('type','entrée');
        $results = $query->getQuery()->getResult();

        return $results;
    }

    public function findAllSortie()
    {
        $query = $this->createQueryBuilder('o');
        $query
            ->select('SUM(o.operationBanqueMontant) as totalSortie')
            ->where('o.operationBanqueType =:type')
            ->setParameter('type','sortie');
        $results = $query->getQuery()->getResult();

        return $results;
    }
}
