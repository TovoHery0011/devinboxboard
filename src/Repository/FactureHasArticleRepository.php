<?php

namespace App\Repository;

use App\Entity\FactureHasArticle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FactureHasArticle|null find($id, $lockMode = null, $lockVersion = null)
 * @method FactureHasArticle|null findOneBy(array $criteria, array $orderBy = null)
 * @method FactureHasArticle[]    findAll()
 * @method FactureHasArticle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FactureHasArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FactureHasArticle::class);
    }

    // /**
    //  * @return FactureHasArticle[] Returns an array of FactureHasArticle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FactureHasArticle
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
