<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200903071458 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE article (id INT AUTO_INCREMENT NOT NULL, article_libelle VARCHAR(45) NOT NULL, article_code VARCHAR(45) NOT NULL, article_qte VARCHAR(45) NOT NULL, article_unite VARCHAR(45) NOT NULL, article_pu DOUBLE PRECISION NOT NULL, article_total DOUBLE PRECISION NOT NULL, article_num_operation VARCHAR(45) NOT NULL, article_nom VARCHAR(45) NOT NULL, article_fournisseur VARCHAR(45) NOT NULL, article_date VARCHAR(45) NOT NULL, article_pu_achat DOUBLE PRECISION NOT NULL, article_pu_vente DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE article');
    }
}
