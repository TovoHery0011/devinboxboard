<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200820172650 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE caisse (id INT AUTO_INCREMENT NOT NULL, caisse_num_operation VARCHAR(45) DEFAULT NULL, caisse_type_operation VARCHAR(255) NOT NULL, caisse_date VARCHAR(45) NOT NULL, caisse_num_facture VARCHAR(45) NOT NULL, caisse_libelle VARCHAR(45) NOT NULL, caisse_montant DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE charge (id INT AUTO_INCREMENT NOT NULL, charge_num_operation VARCHAR(45) DEFAULT NULL, charge_nature VARCHAR(45) NOT NULL, cahrge_date VARCHAR(255) NOT NULL, charge_echeance VARCHAR(45) NOT NULL, charge_libelle VARCHAR(45) NOT NULL, charge_montant VARCHAR(45) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE operation_banque (id INT AUTO_INCREMENT NOT NULL, operation_banque_num VARCHAR(45) DEFAULT NULL, operation_banque_type VARCHAR(255) NOT NULL, operation_banque_date VARCHAR(255) NOT NULL, operation_banque_num_facture VARCHAR(45) NOT NULL, operation_banque_libelle VARCHAR(45) NOT NULL, operation_banque_montant DOUBLE PRECISION NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE caisse');
        $this->addSql('DROP TABLE charge');
        $this->addSql('DROP TABLE operation_banque');
    }
}
